package consoleCommander.interaction;

/**
 * Interface to define characteristics of interactables that use the dark view
 * system.
 * 
 * @author Lumme
 * @version 0.1
 * @since 07-09-2018
 */

public interface DarkViewable {

	/**
	 * @return boolean
	 */

	public boolean isDarkView();

	/**
	 * @param darkView
	 */

	public void setDarkView(boolean darkView);
}
