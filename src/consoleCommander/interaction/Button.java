package consoleCommander.interaction;

import neathDiggers.core.DrawingHelper;
import neathDiggers.core.Settings;
import neathDiggers.entities.Color;
import neathDiggers.soundHandling.SoundKeeper;
import neathDiggers.soundHandling.types.SfxType;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PFont;

/**
 * Abstract class to define buttons.
 * 
 * @author Lumme
 * @version 0.1
 * @since 20-08-2018
 */

public abstract class Button extends Interactable implements DarkViewable {
	private String label = "";
	private int textSize = Settings.DEFAULT_TEXT_SIZE;
	private PFont font;

	private Color line = Settings.DEFAULT_LINE_COLOR;
	private Color over = Settings.DEFAULT_OVER_COLOR;
	private Color back = Settings.DEFAULT_BACK_COLOR;
	private Color txtc = Settings.DEFAULT_TXTC_COLOR;
	private Color darkTxtc = Settings.DEFAULT_DARK_TXTC_COLOR;
	private Color darkBack = Settings.DEFAULT_DARK_BACK_COLOR;
	private Color darkLine = Settings.DEFAULT_DARK_LINE_COLOR;

	private boolean darkView = false;

	/**
	 * Constructor. The inputs x, y, w and h are assigned to the super constructor.
	 * The variable p is set to the public variable instance in the Main class.
	 * 
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 */

	public Button(int x, int y, int w, int h, PApplet p) {
		super(x, y, w, h, p);
	}

	/**
	 * Constructor. The inputs x, y, w and h are assigned to the super constructor.
	 * The input label is assigned to the variable. The variable p is set to the
	 * public variable instance in the Main class.
	 * 
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param label
	 */

	public Button(int x, int y, int w, int h, PApplet p, String label, PFont font) {
		super(x, y, w, h, p);
		this.label = label;
		this.font = font;
	}

	// DRAW

	/**
	 * Draws interactable when the super class variable active is true. It draws a
	 * rectangle using the x, y, w and h variables from the super object. It is uses
	 * the default colors from the Settings class for outline, mouse over, and
	 * background. It draws the text in the label variable with mainFont from the
	 * Main class as the font, textSize as the text size and the default text color
	 * from the Settings class as the color. The text is centered both vertically
	 * and horizontally.
	 */

	@Override
	public void draw() {
		if (isActive()) {
			if (!darkView) {
				getP().stroke(DrawingHelper.getPColor(getP(), line, getTransparency()));
				if (isMouseOver()) {
					getP().fill(DrawingHelper.getPColor(getP(), over, getTransparency()));
				} else {
					getP().fill(back.getR(), back.getG(), back.getB(), (float) (back.getA() * getTransparency()));
				}
			} else {
				getP().stroke(DrawingHelper.getPColor(getP(), darkLine, getTransparency()));
				getP().fill(DrawingHelper.getPColor(getP(), darkBack, getTransparency()));
			}

			getP().rect(getX(), getY(), getW(), getH());

			if (label != null && !label.equals("")) {
				getP().textFont(font);
				getP().textSize(textSize);
				getP().textAlign(PConstants.CENTER, PConstants.CENTER);
				if (!darkView) {
					getP().fill(DrawingHelper.getPColor(getP(), txtc, getTransparency()));
				} else {
					getP().fill(DrawingHelper.getPColor(getP(), darkTxtc, getTransparency()));
				}
				getP().text(label, getX() + getW() / 2 + 2, getY() + getH() / 2 + 1);
			}
		}
	}

	// INTERACTIONS

	/**
	 * Method for behaviour when mouse button is clicked.
	 */

	@Override
	public void mouseClicked() {
		if (isMouseOver() && isActive() && !darkView) {
			SoundKeeper.playSFX(SfxType.BUTTON, 0.4);
			action();
		}
	}

	/**
	 * Abstract method for behaviour. Is being run when mouse button is clicked.
	 */

	public abstract void action();

	// GETTERS AND SETTERS

	/**
	 * @return String
	 */

	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 */

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return boolean
	 */

	@Override
	public boolean isDarkView() {
		return darkView;
	}

	/**
	 * @param darkView
	 */

	@Override
	public void setDarkView(boolean darkView) {
		this.darkView = darkView;
	}

	/**
	 * @return int
	 */

	public int getTextSize() {
		return textSize;
	}

	/**
	 * @param textSize
	 */

	public void setTextSize(int textSize) {
		this.textSize = textSize;
	}

	/**
	 * @return Color
	 */

	public Color getLine() {
		return line;
	}

	/**
	 * @param line
	 */

	public void setLine(Color line) {
		this.line = line;
	}

	/**
	 * @return Color
	 */

	public Color getOver() {
		return over;
	}

	/**
	 * @param over
	 */

	public void setOver(Color over) {
		this.over = over;
	}

	/**
	 * @return Color
	 */

	public Color getBack() {
		return back;
	}

	/**
	 * @param back
	 */

	public void setBack(Color back) {
		this.back = back;
	}

	/**
	 * @return Color
	 */

	public Color getTxtc() {
		return txtc;
	}

	/**
	 * @param txtc
	 */

	public void setTxtc(Color txtc) {
		this.txtc = txtc;
	}

	/**
	 * @return Color
	 */

	public Color getDarkTxtc() {
		return darkTxtc;
	}

	/**
	 * @param darkTxtc
	 */

	public void setDarkTxtc(Color darkTxtc) {
		this.darkTxtc = darkTxtc;
	}

	/**
	 * @return Color
	 */

	public Color getDarkBack() {
		return darkBack;
	}

	/**
	 * @param darkBack
	 */

	public void setDarkBack(Color darkBack) {
		this.darkBack = darkBack;
	}

	/**
	 * @return Color
	 */

	public Color getDarkLine() {
		return darkLine;
	}

	/**
	 * @param darkLine
	 */

	public void setDarkLine(Color darkLine) {
		this.darkLine = darkLine;
	}

	/**
	 * @param font
	 */

	public void setFont(PFont font) {
		this.font = font;
	}

	/**
	 * @return PFont
	 */

	public PFont getFont() {
		return font;
	}
}
