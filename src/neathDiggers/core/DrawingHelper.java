package neathDiggers.core;

import lumCode.utils.ExMath;
import neathDiggers.entities.Color;
import neathDiggers.entities.ColorModifier;
import processing.core.PApplet;

/**
 * Static final class used to intercept color changing for drawing for the
 * purpose of have global color modifiers
 * 
 * @author Lumme
 * @version 0.1
 * @since 14-08-2019
 */

public final class DrawingHelper {
	private static ColorModifier multiplier = new ColorModifier(1.0, 1.0);
	private static ColorModifier adder = new ColorModifier(0.0, 0.0);
	private static ColorModifier maximum = new ColorModifier(255.0, 255.0);
	private static ColorModifier minimum = new ColorModifier(0.0, 0.0);
	private static ColorModifier alignColor = new ColorModifier(0.0, 0.0, 0.0, 255.0);
	private static double alignPercentage = 0.00;

	/**
	 * Returns a given color with its channels multiplied with the channels of the
	 * ColorModifier multiplier and added with the channels of the ColorModifier
	 * adder. Should always be used when setting the color of a PApplet with the
	 * exception of drawing the background.
	 * 
	 * @param p
	 * @param r
	 * @param g
	 * @param b
	 * @param a
	 * @return int representing the color
	 */

	public static int getPColor(PApplet p, int r, int g, int b, int a) {
		if (alignPercentage < 0.01) {
			return p.color(
					(int) (r == 0 ? 0
							: ExMath.clamp(r * multiplier.getR() + adder.getR(), minimum.getR(), maximum.getR())),
					(int) (g == 0 ? 0
							: ExMath.clamp(g * multiplier.getG() + adder.getG(), minimum.getG(), maximum.getG())),
					(int) (b == 0 ? 0
							: ExMath.clamp(b * multiplier.getB() + adder.getB(), minimum.getB(), maximum.getB())),
					(int) (a == 0 ? 0
							: ExMath.clamp(a * multiplier.getA() + adder.getA(), minimum.getA(), maximum.getA())));
		} else {
			return p.lerpColor(p.color(
					(int) (r == 0 ? 0
							: ExMath.clamp(r * multiplier.getR() + adder.getR(), minimum.getR(), maximum.getR())),
					(int) (g == 0 ? 0
							: ExMath.clamp(g * multiplier.getG() + adder.getG(), minimum.getG(), maximum.getG())),
					(int) (b == 0 ? 0
							: ExMath.clamp(b * multiplier.getB() + adder.getB(), minimum.getB(), maximum.getB())),
					(int) (a == 0 ? 0
							: ExMath.clamp(a * multiplier.getA() + adder.getA(), minimum.getA(), maximum.getA()))),
					p.color((int) alignColor.getR(), (int) alignColor.getG(), (int) alignColor.getB(),
							(int) alignColor.getA()),
					(float) alignPercentage);
		}
	}

	/**
	 * Overloaded method that has the channels of the given color combined.
	 * 
	 * @param p
	 * @param col
	 * @return int representing the color
	 */

	public static int getPColor(PApplet p, Color col) {
		return getPColor(p, col.getR(), col.getG(), col.getB(), col.getA());
	}

	/**
	 * Overloaded method that multiplies the a channel with a given transparency.
	 * 
	 * @param p
	 * @param col
	 * @param transparency
	 * @return int representing the color
	 */

	public static int getPColor(PApplet p, Color col, double transparency) {
		return getPColor(p, col.getR(), col.getG(), col.getB(), (int) (col.getA() * transparency));
	}

	/**
	 * Overloaded method that has the channels of the given color separated with the
	 * exception of the a channel which is assumed to be 255 (fully visible).
	 * 
	 * @param p
	 * @param r
	 * @param g
	 * @param b
	 * @return int representing the color
	 */

	public static int getPColor(PApplet p, int r, int g, int b) {
		return getPColor(p, r, g, b, 255);
	}

	/**
	 * @return the multiplier
	 */

	public static ColorModifier getMultiplier() {
		return multiplier;
	}

	/**
	 * @param multiplier
	 */

	public static void setMultiplier(ColorModifier multiplier) {
		DrawingHelper.multiplier = multiplier;
	}

	/**
	 * @return the adder
	 */

	public static ColorModifier getAdder() {
		return adder;
	}

	/**
	 * @param adder
	 */

	public static void setAdder(ColorModifier adder) {
		DrawingHelper.adder = adder;
	}

	/**
	 * @return the maximum
	 */

	public static ColorModifier getMaximum() {
		return maximum;
	}

	/**
	 * @param maximum
	 */

	public static void setMaximum(ColorModifier maximum) {
		DrawingHelper.maximum = maximum;
	}

	/**
	 * @return the minimum
	 */

	public static ColorModifier getMinimum() {
		return minimum;
	}

	/**
	 * @param minimum
	 */

	public static void setMinimum(ColorModifier minimum) {
		DrawingHelper.minimum = minimum;
	}

	/**
	 * @return the align color
	 */

	public static ColorModifier getAlignColor() {
		return alignColor;
	}

	/**
	 * @param alignColor
	 */

	public static void setAlignColor(ColorModifier alignColor) {
		DrawingHelper.alignColor = alignColor;
	}

	/**
	 * @return the align percentage
	 */

	public static double getAlignPercentage() {
		return alignPercentage;
	}

	/**
	 * @param alignPercentage
	 */

	public static void setAlignPercentage(double alignPercentage) {
		DrawingHelper.alignPercentage = alignPercentage;
	}

	/**
	 * Resets the multiplier to the initial value
	 */

	public static void resetMultiplier() {
		multiplier = new ColorModifier(1.0, 1.0);
	}

	/**
	 * Resets the adder to the initial value
	 */

	public static void resetAdder() {
		adder = new ColorModifier(0.0, 0.0);
	}

	/**
	 * Resets the maximum to the initial value
	 */

	public static void resetMaximum() {
		maximum = new ColorModifier(255.0, 255.0);
	}

	/**
	 * Resets the minimum to the initial value
	 */

	public static void resetMinimum() {
		minimum = new ColorModifier(0.0, 0.0);
	}

	/**
	 * Resets the align color to the initial value
	 */

	public static void resetAlign() {
		alignColor = new ColorModifier(0.0, 0.0, 0.0, 255.0);
	}
}
