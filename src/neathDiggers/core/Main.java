package neathDiggers.core;

import neathDiggers.keys.Key;
import neathDiggers.keys.KeyTracker;
import processing.core.PApplet;
import processing.core.PFont;
import processing.event.MouseEvent;

public class Main extends PApplet {
	public static PFont mainFont;
	public static boolean running;

	public static void main(String[] args) {
		running = true;
		Thread.currentThread().setName("Screen");

		running = false;
	}

	// --------
	// SETTINGS
	// --------

	@Override
	public void settings() {
		size(640, 480);

		mainFont = loadFont(Settings.DEFAULT_FONT_PATH);

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// Do nothing
		}
	}

	// -----
	// SETUP
	// -----

	@Override
	public void setup() {

		frameRate(Settings.FRAME_RATE);
		surface.setResizable(true);
		surface.setLocation(1940, 20);

	}

	// ----
	// DRAW
	// ----

	@Override
	public void draw() {
		background(0);
	}

	// -----------
	// INTERACTION
	// -----------

	@Override
	public void mousePressed() {
		if (mouseButton == LEFT) {

		}
	}

	@Override
	public void mouseWheel(MouseEvent event) {
	}

	@Override
	public void keyPressed() {
		setKey(true);
	}

	@Override
	public void keyReleased() {
		setKey(false);
	}

	private void setKey(boolean state) {
		if (key == CODED) {
			if (keyCode == LEFT) {
				KeyTracker.state(Key.LEFT, state);
			} else if (keyCode == UP) {
				KeyTracker.state(Key.UP, state);
			} else if (keyCode == RIGHT) {
				KeyTracker.state(Key.RIGHT, state);
			} else if (keyCode == DOWN) {
				KeyTracker.state(Key.DOWN, state);
			} else if (keyCode == ENTER) {
				KeyTracker.state(Key.ENTER, state);
			}
		} else {
			if (key == 'a' || key == 'A') {
				KeyTracker.state(Key.LEFT, state);
			} else if (key == 'w' || key == 'W') {
				KeyTracker.state(Key.UP, state);
			} else if (key == 'd' || key == 'D') {
				KeyTracker.state(Key.RIGHT, state);
			} else if (key == 's' || key == 'S') {
				KeyTracker.state(Key.DOWN, state);
			} else if (key == ENTER) {
				KeyTracker.state(Key.ENTER, state);
			}
		}
	}

	// ---------
	// FUNCTIONS
	// ---------

	// ---------
	// UTILITIES
	// ---------

	// -------------------
	// GETTERS AND SETTERS
	// -------------------

	public PApplet getPApplet() {
		return this;
	}

	public static boolean isRunning() {
		return running;
	}
}
