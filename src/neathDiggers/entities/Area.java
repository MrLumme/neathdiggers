package neathDiggers.entities;

public class Area extends Position {

	private int w, h;

	public Area(int x, int y, int w, int h) {
		super(x, y);
		this.w = w;
		this.h = h;
	}

	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public boolean isPositionInArea(Position position) {
		boolean out = false;
		if (position.getX() > getX() && position.getX() < (getX() + w) && position.getY() > getY()
				&& position.getY() < (getY() + h)) {
			out = true;
		}
		return out;
	}

	public boolean isPositionInArea(int x, int y) {
		boolean out = false;
		if (x > getX() && x < (getX() + w) && y > getY() && y < (getY() + h)) {
			out = true;
		}
		return out;
	}
}
