package neathDiggers.entities;

public class ColorModifier {
	private double r, g, b, a;

	public ColorModifier(double grey, double a) {
		r = grey;
		g = grey;
		b = grey;
		this.a = a;
	}

	public ColorModifier(double r, double g, double b, double a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	// GETTERS AND SETTERS

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	public double getG() {
		return g;
	}

	public void setG(double g) {
		this.g = g;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}
}
