package neathDiggers.entities;

public class Position {

	private int x, y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public static Position getMiddle(Position a, Position b) {

		int x = Math.abs(a.getX() - b.getX());
		int y = Math.abs(a.getY() - b.getY());

		return new Position(x, y);
	}
}
