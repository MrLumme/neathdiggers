package neathDiggers.interaction.enums;

public enum ButtonType {
	RECTANGLE, ELIPSE, TRIANGLE_UP, TRIANGLE_DOWN, TRIANGLE_LEFT, TRIANGLE_RIGHT;
}
