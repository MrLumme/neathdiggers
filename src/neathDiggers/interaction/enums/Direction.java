package neathDiggers.interaction.enums;

public enum Direction {
	LEFT, UP, RIGHT, DOWN;
}
