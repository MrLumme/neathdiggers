package neathDiggers.soundHandling.types;

import java.io.File;

import neathDiggers.core.Settings;

public enum SfxType {
	DAMAGE, BUTTON;

	public static File getFile(SfxType type) {
		return new File(Settings.AUDIO_PATH + "sfx/sfx_" + type.name().toLowerCase() + ".wav");
	}
}
