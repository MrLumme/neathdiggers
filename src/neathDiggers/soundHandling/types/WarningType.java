package neathDiggers.soundHandling.types;

public enum WarningType {
	BATTERY_CRITICAL, OXYGEN_CRITICAL, COMPONENT_DESTROYED
}
