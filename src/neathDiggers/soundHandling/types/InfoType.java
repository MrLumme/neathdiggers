package neathDiggers.soundHandling.types;

public enum InfoType {
	BATTERY_LEVEL, BATTERY_MAX, OXYGEN_LEVEL, OXYGEN_MAX;
}
