package neathDiggers.soundHandling.entities;

import java.io.File;

import neathDiggers.soundHandling.types.InfoType;

public class InfoSound extends Sound {
	private InfoType type;

	public InfoSound(InfoType type) {
		this.type = type;
	}

	public InfoType getType() {
		return type;
	}

	public void setType(InfoType type) {
		this.type = type;
	}

	@Override
	public File[] getPlaySetup() {
		File[] out = null;

		// Make stup

		return out;
	}
}
