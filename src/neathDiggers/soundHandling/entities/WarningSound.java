package neathDiggers.soundHandling.entities;

import java.io.File;

import neathDiggers.soundHandling.types.WarningType;

public class WarningSound extends Sound {
	private WarningType type;

	public WarningSound(WarningType type) {
		this.type = type;
	}

	public WarningType getType() {
		return type;
	}

	public void setType(WarningType type) {
		this.type = type;
	}

	@Override
	public File[] getPlaySetup() {
		File[] out = null;

		// Make setup

		return out;
	}
}
