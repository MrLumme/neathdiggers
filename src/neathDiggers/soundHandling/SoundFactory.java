package neathDiggers.soundHandling;

import java.io.File;

import neathDiggers.core.Settings;
import neathDiggers.soundHandling.entities.InfoSound;
import neathDiggers.soundHandling.entities.WarningSound;
import neathDiggers.soundHandling.types.InfoType;
import neathDiggers.soundHandling.types.WarningType;

/**
 * Static class to generate or fetch sounds.
 * 
 * @author Lumme
 * @version 0.1
 * @since 24-09-2019
 */

public final class SoundFactory {

	// WARNING TYPES

	/**
	 * Generates a warning sound from either of battery or oxygen being at low
	 * capacity. Does not do both.
	 * 
	 * @param batteryLow
	 * @param oxygenLow
	 * @return the generated WarningSound, or null if it could not be generated
	 */

	public static WarningSound generateWarningSound(boolean batteryLow, boolean oxygenLow) {
		if (batteryLow) {
			return new WarningSound(WarningType.BATTERY_CRITICAL);
		} else if (oxygenLow) {
			return new WarningSound(WarningType.OXYGEN_CRITICAL);
		} else {
			return null;
		}
	}

	// INFO TYPES

	/**
	 * Generates an info sound from either of battery or oxygen being at maximum
	 * capacity. Does not do both.
	 * 
	 * @param batteryMax
	 * @param oxygenMax
	 * @return the generated InfoSound, or null if it could not be generated
	 */

	public static InfoSound generateInfoSound(boolean batteryMax, boolean oxygenMax) {
		if (batteryMax) {
			return new InfoSound(InfoType.BATTERY_MAX);
		} else if (oxygenMax) {
			return new InfoSound(InfoType.OXYGEN_MAX);
		} else {
			return null;
		}
	}

	/**
	 * Returns the song after a given song in the music folder located at the audio
	 * path.
	 * 
	 * @param song
	 * @return the next song as a File, or null if it could not be found
	 */

	public static File getNewBackground(AudioPlayer song) {
		File[] pool = new File(Settings.AUDIO_PATH + "music/").listFiles();
		File out = null;
		if (song != null) {
			for (int i = 0; i < pool.length; i++) {
				if (pool[i].getAbsolutePath().equals(song.getSounds()[0].getAbsolutePath())) {
					if ((i + 1) >= pool.length) {
						out = pool[0];
						break;
					} else {
						out = pool[i + 1];
						break;
					}
				}
			}
		} else if (pool.length > 0) {
			out = pool[0];
		}
		return out;
	}
}
