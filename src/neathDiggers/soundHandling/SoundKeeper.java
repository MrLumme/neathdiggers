package neathDiggers.soundHandling;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import neathDiggers.core.Main;
import neathDiggers.core.Settings;
import neathDiggers.soundHandling.entities.InfoSound;
import neathDiggers.soundHandling.entities.Sound;
import neathDiggers.soundHandling.entities.WarningSound;
import neathDiggers.soundHandling.types.SfxType;

/**
 * Static class to manage sounds. Extends Thread.
 * 
 * @author Lumme
 * @version 0.1
 * @since 24-09-2019
 */

public final class SoundKeeper extends Thread {
	private static final int BUFFER_SIZE = 128000;

	public static SoundKeeper instance = null;

	private static ArrayList<AudioPlayer> effects = new ArrayList<>();
	private static AudioPlayer info = null;
	private static AudioPlayer song = null;
	private static boolean muted = false;
	private static double volume = 0.5;

	private static final ArrayList<WarningSound> warningQueue = new ArrayList<>();
	private static final ArrayList<InfoSound> infoQueue = new ArrayList<>();

	/**
	 * Constructor. Can not be used outside to class. Sets the class to be a daemon
	 * thread.
	 */

	private SoundKeeper() {
		setDaemon(true);
		setName("SoundKeeper");
	}

	/**
	 * Overridden method of super class Thread. Main running method.
	 */

	@Override
	public void run() {
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			// Do nothing
		}

		File sd = new File(Settings.AUDIO_PATH);
		if (sd.exists()) {
			if (!muted) {
				AudioPlayer st = new AudioPlayer(new File(Settings.AUDIO_PATH + "male_start.wav"));
				st.start();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// Do nothing
				}
			}
			while (Main.isRunning()) {
				if (!muted) {
					// Play info audio
					if (info == null) {
						if (!warningQueue.isEmpty()) {
							WarningSound war = warningQueue.remove(0);
							war.setPlaying(true);
							info = new AudioPlayer(war.getPlaySetup());
							info.setVol(volume);
							info.start();
						} else if (!infoQueue.isEmpty()) {
							InfoSound inf = infoQueue.remove(0);
							inf.setPlaying(true);
							info = new AudioPlayer(inf.getPlaySetup());
							info.setVol(volume);
							info.start();
						}
					} else if (!info.isAlive() || info.isHalted()) {
						info = null;
					} else if (info.isErrored() && info.isAlive()) {
						info.halt();
					}

					// Play background
					if (song == null || !song.isAlive() || song.isHalted()) {
						File next = SoundFactory.getNewBackground(song);
						if (next != null) {
							song = new AudioPlayer(next);
							song.setVol(volume / 8);
							song.start();
						}
					} else if (song.isErrored() && song.isAlive()) {
						song.halt();
					}

					try {
						Thread.sleep(Settings.SOUND_DELAY);
					} catch (InterruptedException e) {
						// Do nothing
					}
				} else {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// Do nothing
					}
				}
			}
		}
	}

	/**
	 * Initializes the the class by getting the instance and starting it, before
	 * returning the instance.
	 * 
	 * @return SoundKeeper
	 */

	public static SoundKeeper init() {
		SoundKeeper o = getInstance();
		o.start();
		return o;
	}

	/**
	 * Returns the instance of the class.
	 * 
	 * @return SoundKeeper
	 */

	public static SoundKeeper getInstance() {
		if (instance == null) {
			instance = new SoundKeeper();
			instance.setDaemon(true);
		}
		return instance;
	}

	/**
	 * Adds a Sound to the correct queue
	 * 
	 * @param warning
	 */

	public static void addSound(Sound sound) {
		if (sound != null) {
			if (sound instanceof WarningSound) {
				warningQueue.add((WarningSound) sound);
			} else if (sound instanceof InfoSound) {
				infoQueue.add((InfoSound) sound);
			}
		}
	}

	/**
	 * Plays a given sound effect type at a given volume.
	 * 
	 * @param type
	 * @param type
	 */

	public static void playSFX(SfxType type, double vol) {
		if (!muted) {
			Iterator<AudioPlayer> it = effects.iterator();
			while (it.hasNext()) {
				AudioPlayer sfx = it.next();
				if (!sfx.isAlive() || sfx.isHalted()) {
					it.remove();
				} else if (sfx.isErrored()) {
					sfx.halt();
					it.remove();
				}
			}
			if (effects.size() < Settings.SFX_LIMIT) {
				AudioPlayer sfx = new AudioPlayer(SfxType.getFile(type));
				sfx.setVol(vol * volume);
				sfx.start();
				effects.add(sfx);
			}
		}
	}

	/**
	 * Overloaded method that plays a given sfx type at 60% volume.
	 * 
	 * @param type
	 */

	public static void playSFX(SfxType type) {
		playSFX(type, 0.6);
	}

	/**
	 * @return boolean
	 */

	public static boolean isMuted() {
		return muted;
	}

	/**
	 * @param muted
	 */

	public static void setMuted(boolean muted) {
		SoundKeeper.muted = muted;
	}

	/**
	 * @return double
	 */

	public static double getVolume() {
		return volume;
	}

	/**
	 * @param muted
	 */

	public static void setVolume(double volume) {
		SoundKeeper.volume = volume;
	}
}
