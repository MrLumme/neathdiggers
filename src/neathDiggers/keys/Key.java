package neathDiggers.keys;

/**
 * Enum used to define functional game keys.
 * 
 * @author Lumme
 * @version 0.1
 * @since 17-07-2019
 */

public enum Key {
	LEFT, RIGHT, UP, DOWN, ENTER;

	/**
	 * Method used by KeyTracker to get the enums as a predefined number.
	 * 
	 * @param key
	 * @return int
	 */

	static int toInt(Key key) {
		int out = -1;

		switch (key) {
		case DOWN:
			out = 0;
			break;
		case LEFT:
			out = 1;
			break;
		case RIGHT:
			out = 2;
			break;
		case UP:
			out = 3;
			break;
		case ENTER:
			out = 4;
			break;
		}

		return out;
	}

	/**
	 * Return the amount of functional keys tracked.
	 * 
	 * @return int
	 */

	static int getCount() {
		return 5;
	}
}
