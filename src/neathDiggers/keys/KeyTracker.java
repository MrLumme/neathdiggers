package neathDiggers.keys;

/**
 * Static class used keep track of keys. It is needed because multiple keys can
 * result in the same action. For example would 'a' and left arrow be treated
 * the same in the game.
 * 
 * @author Lumme
 * @version 0.1
 * @since 17-07-2019
 */

public final class KeyTracker {
	private static final boolean keys[] = new boolean[Key.getCount()];

	/**
	 * Updates a given key with a given state
	 * 
	 * @param key
	 * @param state
	 */

	public static void state(Key key, boolean state) {
		keys[key.toInt(key)] = state;
	}

	/**
	 * Returns the state of a given key
	 * 
	 * @param key
	 * @return
	 */

	public static boolean getState(Key key) {
		return keys[Key.toInt(key)];
	}
}
